''' Script for writing PDB and PSF files (using the psfgen 
with VMD) from ASE atoms objects and CHARMM topology files.
'''
import os
import tempfile
import numpy as np

class PSFPy():
    def __init__(self, topology, tag_dict):
        if type(topology) == str:
            self.topology = [topology]
        else:
            self.topology = topology
        self.tag_dict = tag_dict

    def write_pdb_psf(self, atoms, pdb_file='out.pdb', psf_file='out.psf',
                      verbose=True):

        # Write PDB for psfgen
        temp_pdb = tempfile.NamedTemporaryFile(mode='w+b')
        self.write_proteindatabank(temp_pdb, atoms)
        temp_pdb.flush()

        # Run psfgen
        with tempfile.NamedTemporaryFile(mode='w+b') as f:
            f.write('package require psfgen\n')
            for topo in self.topology:
                f.write('topology %s\n' % topo)
            f.write('mol load pdb %s\n' % temp_pdb.name)

            tags = atoms.get_tags()
            counter = 0
            segment = 0
            while counter < len(atoms):
                tag = tags[counter]
                resi = self.tag_dict[tag][0]
                n = len(self.tag_dict[tag][1]) 
                frag = resi.lower()
                pdb = '.tmp_%d.pdb' % counter
                with open(pdb, 'w') as g:
                    self.write_proteindatabank(g, atoms[counter:counter+n])
                f.write('segment %s {pdb %s}\n' % (segment, pdb))
                f.write('coordpdb %s %s\n' % (pdb, segment))
                counter += n 
                segment += 1

            f.write('writepdb %s\n' % pdb_file) 
            f.write('writepsf %s\n' % psf_file)
            f.write('quit\n')
            f.flush()

            cmd = 'vmd -dispdev text -e %s' % f.name
            if not verbose:
                cmd += ' > /dev/null'
            os.system(cmd)
            os.system('rm .tmp_*.pdb')

        if atoms.get_pbc().any():
            # It seems psfgen doesn't write the CRYST header
            os.system('head -1 %s | cat - %s > tmp && mv tmp %s' % \
                      (temp_pdb.name, pdb_file, pdb_file))

        temp_pdb.close()
        return

    def write_proteindatabank(self, fileobj, images):
        # modified from ase.io.proteindatabank.write_proteindatabank()

        if hasattr(images, 'get_positions'):
            images = [images]

        rotation = None
        if images[0].get_pbc().any():
            from ase.geometry import cell_to_cellpar, cellpar_to_cell

            currentcell = images[0].get_cell()
            cellpar = cell_to_cellpar(currentcell)
            exportedcell = cellpar_to_cell(cellpar)
            rotation = np.linalg.solve(currentcell, exportedcell)
            # ignoring Z-value, using P1 since we have all atoms defined explicitly
            format = 'CRYST1%9.3f%9.3f%9.3f%7.2f%7.2f%7.2f P 1\n'
            fileobj.write(format % (cellpar[0], cellpar[1], cellpar[2],
                                    cellpar[3], cellpar[4], cellpar[5]))

        #     1234567 123 6789012345678901   89   67   456789012345678901234567 890
        format = ('ATOM  %5d %4s %4s %3d    %8.3f%8.3f%8.3f  1.00  0.00'
                  '          %2s  \n')

        # RasMol complains if the atom index exceeds 100000. There might
        # be a limit of 5 digit numbers in this field.
        MAXNUM = 100000

        for i, atoms in enumerate(images):
            fileobj.write('MODEL     ' + str(i + 1) + '\n')

            pos = atoms.get_positions()
            if rotation is not None:
                pos = pos.dot(rotation)

            tags = atoms.get_tags()
            tag_counters = {tag:0 for tag in tags}
            for j,atom in enumerate(atoms):
                tag = tags[j]
                x = self.tag_dict[tag]
                residue_name = x[0]
                atom_index = tag_counters[tag] % len(x[1])
                atom_name = x[1][atom_index]

                residue_index = 1
                for t in list(set(tags)):
                    n = len(self.tag_dict[t][1])
                    residue_index += tag_counters[t] / n

                tag_counters[tag] += 1

                x, y, z = pos[j]
                fileobj.write(format % (j % MAXNUM, atom_name, residue_name, 
                                        residue_index, x, y, z, atom.symbol))
            fileobj.write('ENDMDL\n')

        return
