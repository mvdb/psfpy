from ase.build import molecule
from psfpy import PSFPy

# Create a water molecule in a box
atoms = molecule('H2O')
atoms.center(vacuum=6)
atoms.set_pbc(True)

# Give all water atoms the same tag (here: 0)
atoms.set_tags([0]*3)

# Set up PSFPy; assumes the toppar_c36_jul17.tgz 
# file has been downloaded and extracted (from
# http://mackerell.umaryland.edu/charmm_ff.shtml)
p = PSFPy('./toppar/toppar_water_ions.str', 
          {0:('TIP3', ['OH2', 'H1', 'H2'])})
# The order of 'OH2', 'H1', 'H2' should match 
# the order in the Atoms object as well as the definition
# of the residue (TIP3) in the topology file 

# Write the PDF and PSF files 
p.write_pdb_psf(atoms,  pdb_file='out.pdb', psf_file='out.psf')
